namespace IdentityLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateUserandAddPost : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        PostId = c.String(nullable: false, maxLength: 128),
                        Title = c.String(),
                        Slug = c.String(),
                        Content = c.String(),
                        AuthorUserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.PostId)
                .ForeignKey("dbo.AspNetUsers", t => t.AuthorUserId, cascadeDelete: true)
                .Index(t => t.AuthorUserId);
            
            AddColumn("dbo.AspNetUsers", "FullName", c => c.String());
            AddColumn("dbo.AspNetUsers", "BirthDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Posts", "AuthorUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Posts", new[] { "AuthorUserId" });
            DropColumn("dbo.AspNetUsers", "BirthDate");
            DropColumn("dbo.AspNetUsers", "FullName");
            DropTable("dbo.Posts");
        }
    }
}
