﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityLibrary.Models {
	public class Post {
		public string PostId { get; set; }
		public string Title { get; set; }
		public string Slug { get; set; }
		public string Content { get; set; }
		public string AuthorUserId { get; set; }
		public virtual ApplicationUser User { get; set; }
	}
}
