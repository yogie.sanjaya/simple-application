﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IdentityLibrary.Models {
	public class ApplicationUser : IdentityUser {

		public ApplicationUser() {
			Posts = new List<Post>();
		}

		public string FullName { get; set; }
		public DateTime BirthDate { get; set; }
		public virtual ICollection<Post> Posts { get; set; }

		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager) {
			// Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
			var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
			// Add custom user claims here
			return userIdentity;
		}
	}

	public class ApplicationDbContext : IdentityDbContext<ApplicationUser> {
		public ApplicationDbContext()
			: base("DefaultConnection") {
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder) {
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<ApplicationUser>()
				.HasMany(app => app.Posts).WithRequired(req => req.User).HasForeignKey(fk => fk.AuthorUserId);
		}

		public static ApplicationDbContext Create() {
			return new ApplicationDbContext();
		}

		public DbSet<Post> Posts { get; set; }
	}
}
