﻿using IdentityLibrary.DataAccess;
using IdentityLibrary.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityLibrary.BusinessLogic {
	public class AccountLogic : IAccountLogic {
		private ApplicationDbContext db = new ApplicationDbContext();
		private ApplicationSignInManager SignInManager;
		private ApplicationUserManager UserManager;

		public AccountLogic(ApplicationUserManager userManager, ApplicationSignInManager signInManager) {
			UserManager = userManager;
			SignInManager = signInManager;
		}

		public async Task<bool> RegisterLogic(RegisterViewModel model) {
			var user = new ApplicationUser { UserName = model.UserName, FullName = $"{model.FirstName} {model.LastName}", BirthDate = model.BirthDate, PhoneNumber = model.PhoneNumber, Email = model.Email };
			var result = await UserManager.CreateAsync(user, model.Password);
			if (result.Succeeded) {
				await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
				return true;
			}

			//AddErrors(result);

			return false;
		}

		public async Task<SignInStatus> LoginLogic(LoginViewModel model) {
			return await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
		}

		public List<ApplicationUser> GetAllLogic() {
			var listUser = UserManager.Users.ToList();
			return listUser;
		}

		public async Task<ApplicationUser> GetByIDLogic(string id) {
			var user = await UserManager.FindByIdAsync(id);
			return user;
		}

		public async Task<bool> CreateLogic(AccountViewModel model) {
			var user = new ApplicationUser { UserName = model.UserName, FullName = model.FullName, BirthDate = model.BirthDate, PhoneNumber = model.PhoneNumber, Email = model.Email };
			var result = await UserManager.CreateAsync(user, model.Password);
			if (result.Succeeded) {
				return true;
			}
			return false;
		}

		public async Task<ApplicationUser> UpdateLogic(ApplicationUser model) {
			var user = await UserManager.FindByIdAsync(model.Id);
			user.UserName = model.UserName;
			user.FullName = model.FullName;
			user.Email = model.Email;
			user.PhoneNumber = model.PhoneNumber;
			UserManager.Update(user);
			return user;
		}

		public async Task<IdentityResult> DeleteLogic(string id) {
			var result = IdentityResult.Success;
			var user = await UserManager.FindByIdAsync(id);
			var userLogins = user.Logins;
			var userRoles = await UserManager.GetRolesAsync(id);
			var userClaims = await UserManager.GetClaimsAsync(id);

			using (var transaction = db.Database.BeginTransaction()) {
				foreach (var login in userLogins.ToList()) {
					await UserManager.RemoveLoginAsync(login.UserId, new UserLoginInfo(login.LoginProvider, login.ProviderKey));
				}
				if (userRoles.Count() > 0) {
					foreach (var item in userRoles.ToList()) {
						result = await UserManager.RemoveFromRoleAsync(user.Id, item);
					}
				}
				if (userClaims.Count() > 0) {
					foreach (var item in userClaims.ToList()) {
						result = await UserManager.RemoveClaimAsync(user.Id, item);
					}
				}
				result = await UserManager.DeleteAsync(user);
				transaction.Commit();
			}
			return result;
		}
	}
}
