﻿using IdentityLibrary.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IdentityLibrary.BusinessLogic {
	public interface IPostLogic {
		Task<int> CreateLogic(Post model);
		Task<int> DeleteLogic(string id);
		List<Post> GetAllLogic();
		List<Post> GetByAuthorIdLogic(string Id);
		Post GetByPostIdLogic(string Id);
		Task<int> UpdateLogic(Post model);
	}
}