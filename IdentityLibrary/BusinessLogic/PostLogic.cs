﻿using IdentityLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace IdentityLibrary.BusinessLogic {
	public class PostLogic : IPostLogic {
		private ApplicationDbContext db = new ApplicationDbContext();

		public List<Post> GetAllLogic() {
			var listPost = new List<Post>();
			listPost = db.Posts.ToList();
			return listPost;
		}
		public List<Post> GetByAuthorIdLogic(string Id) {
			var listPost = new List<Post>();
			listPost = db.Posts.Where(x => x.AuthorUserId == Id).ToList();
			return listPost;
		}

		public async Task<int> CreateLogic(Post model) {
			db.Posts.Add(model);
			var result = await db.SaveChangesAsync();
			return result;
		}

		public Post GetByPostIdLogic(string Id) {
			var post = db.Posts.Where(x => x.PostId == Id).FirstOrDefault();
			return post;
		}

		public async Task<int> UpdateLogic(Post model) {
			db.Entry(model).State = EntityState.Modified;
			var result = await db.SaveChangesAsync();
			return result;
		}

		public async Task<int> DeleteLogic(string id) {
			var post = await db.Posts.FindAsync(id);
			db.Posts.Remove(post);
			var result = await db.SaveChangesAsync();
			return result;
		}
	}
}
