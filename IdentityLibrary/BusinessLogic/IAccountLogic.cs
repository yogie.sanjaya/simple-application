﻿using IdentityLibrary.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IdentityLibrary.BusinessLogic {
	public interface IAccountLogic {
		Task<bool> CreateLogic(AccountViewModel model);
		Task<IdentityResult> DeleteLogic(string id);
		List<ApplicationUser> GetAllLogic();
		Task<ApplicationUser> GetByIDLogic(string id);
		Task<SignInStatus> LoginLogic(LoginViewModel model);
		Task<bool> RegisterLogic(RegisterViewModel model);
		Task<ApplicationUser> UpdateLogic(ApplicationUser model);
	}
}