﻿using IdentityLibrary.BusinessLogic;
using IdentityLibrary.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebApp.Controllers {
	public class ManagePostController : Controller {
		private readonly IPostLogic _postLogic;

		public ManagePostController(IPostLogic postLogic) {
			_postLogic = postLogic;
		}

		// GET: ManagePost/Posts/1
		[Authorize]
		public ActionResult Posts(string id) {
			var result = _postLogic.GetByAuthorIdLogic(id);
			return View(result);
		}

		// Get: ManagePost/Create
		[Authorize]
		public ActionResult Create() {
			return View();
		}

		// POST: ManagePost/Create
		[HttpPost]
		public async Task<ActionResult> Create(Post model) {
			if (ModelState.IsValid) {
				model.PostId = Guid.NewGuid().ToString();
				model.AuthorUserId = User.Identity.GetUserId();
				var result = await _postLogic.CreateLogic(model);
				return RedirectToAction("Posts", new { controller = "ManagePost", action = "Posts", id = User.Identity.GetUserId() });
			}
			return View(model);
		}

		// GET: ManagePost/Edit/5
		[Authorize]
		public ActionResult Edit(string id) {
			if (id == null) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var result = _postLogic.GetByPostIdLogic(id);
			if (result == null) {
				return HttpNotFound();
			}
			return View(result);
		}

		// Post: ManagePost/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Edit(Post model) {
			if (ModelState.IsValid) {
				model.AuthorUserId = User.Identity.GetUserId();
				var result = await _postLogic.UpdateLogic(model);
				return RedirectToAction("Posts", new { controller = "ManagePost", action = "Posts", id = User.Identity.GetUserId() });
			}
			return View(model);
		}

		// GET: ApplicationUsers/Delete/5
		[Authorize]
		public ActionResult Delete(string id) {
			if (id == null) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var result = _postLogic.GetByPostIdLogic(id);
			if (result == null) {
				return HttpNotFound();
			}
			return View(result);
		}

		// POST: ApplicationUsers/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> DeleteConfirmed(string id) {
			var result = await _postLogic.DeleteLogic(id);
			return RedirectToAction("Posts", new { controller = "ManagePost", action = "Posts", id = User.Identity.GetUserId() });
		}
	}
}