﻿using IdentityLibrary.BusinessLogic;
using System.Web.Mvc;

namespace WebApp.Controllers {
	public class HomeController : Controller {
		// GET: Home
		public ActionResult Index() {
			var postLogic = new PostLogic();
			var result = postLogic.GetAllLogic();
			return View(result);
		}
	}
}