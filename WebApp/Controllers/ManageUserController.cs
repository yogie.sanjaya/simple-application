﻿using IdentityLibrary.BusinessLogic;
using IdentityLibrary.Models;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebApp.Controllers {
	public class ManageUserController : Controller {
		private readonly IAccountLogic _accountLogic;

		public ManageUserController(IAccountLogic accountLogic) {
			_accountLogic = accountLogic;
		}

		// GET: ManageUser/Users
		[Authorize]
		public ActionResult Users() {
			var users = _accountLogic.GetAllLogic();
			return View(users);
		}

		// Get: ManageUser/Create
		[Authorize]
		public ActionResult Create() {
			return View();
		}

		// POST: ManageUser/Create
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Create(AccountViewModel model) {
			var isSucces = true;
			if (ModelState.IsValid) {
				isSucces = await _accountLogic.CreateLogic(model);
				if (isSucces) {
					return RedirectToAction("Index", "Home");
				} else {
					return View(model);
				}
			}
			return View(model);
		}

		// GET: ManageUser/Edit/5
		[Authorize]
		public async Task<ActionResult> Edit(string id) {
			if (id == null) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var result = await _accountLogic.GetByIDLogic(id);
			if (result == null) {
				return HttpNotFound();
			}
			return View(result);
		}

		// Post: ManageUser/Edit/5
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Edit(ApplicationUser model) {
			if (ModelState.IsValid) {
				var result = await _accountLogic.UpdateLogic(model);
				return RedirectToAction("Users");
			}
			return View(model);
		}

		// GET: ManageUser/Delete/id
		[Authorize]
		public async Task<ActionResult> Delete(string id) {
			if (id == null) {
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var result = await _accountLogic.GetByIDLogic(id);
			if (result == null) {
				return HttpNotFound();
			}
			return View(result);
		}

		// POST: ManageUser/Delete/id
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> DeleteConfirmed(string id) {
			var result = await _accountLogic.DeleteLogic(id);
			return RedirectToAction("Users");
		}
	}
}