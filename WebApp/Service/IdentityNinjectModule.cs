﻿using IdentityLibrary.DataAccess;
using Microsoft.AspNet.Identity.Owin;
using Ninject.Modules;
using System.Web;

namespace WebApp.Service {
	public class IdentityNinjectModule : NinjectModule {
		public override void Load() {
			Kernel.Bind<ApplicationUserManager>().ToMethod(ctx => GetUserManagerFromOwinContext());
			Kernel.Bind<ApplicationSignInManager>().ToMethod(ctx => GetSignInManagerFromOwinContext());
		}

		public static ApplicationUserManager GetUserManagerFromOwinContext() {
			return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
		}

		public static ApplicationSignInManager GetSignInManagerFromOwinContext() {
			return HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
		}
	}
}