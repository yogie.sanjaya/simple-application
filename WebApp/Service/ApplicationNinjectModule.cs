﻿using IdentityLibrary.BusinessLogic;
using Ninject.Modules;

namespace WebApp.Service {
	public class ApplicationNinjectModule : NinjectModule {
		public override void Load() {
			Kernel.Bind<IPostLogic>().To<PostLogic>();
			Kernel.Bind<IAccountLogic>().To<AccountLogic>();
		}
	}
}